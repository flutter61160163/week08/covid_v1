import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questionsValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var questions = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'แน่นหน้าอก',
    'เหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นหรือรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่อไส้อาเจียน',
    'ท้องเสีย',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question.')),
      body: ListView(
        children: [
          CheckboxListTile(
            value: questionsValue[0],
            title: Text(questions[0]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[0] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[1],
            title: Text(questions[1]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[1] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[2],
            title: Text(questions[2]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[2] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[3],
            title: Text(questions[3]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[3] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[4],
            title: Text(questions[4]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[4] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[5],
            title: Text(questions[5]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[5] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[6],
            title: Text(questions[6]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[6] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[7],
            title: Text(questions[7]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[7] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[8],
            title: Text(questions[8]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[8] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[9],
            title: Text(questions[9]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[9] = newValue!;
              });
            },
          ),
          CheckboxListTile(
            value: questionsValue[10],
            title: Text(questions[10]),
            onChanged: (newValue) {
              setState(() {
                questionsValue[10] = newValue!;
              });
            },
          ),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: Text('Save')),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    final prefs = await SharedPreferences.getInstance();
    var stringQuestionValue = prefs.getString('question_value') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrStringQuestionValues = stringQuestionValue
        .substring(1, stringQuestionValue.length - 1)
        .split(',');
    setState(() {
      for (var i = 0; i < arrStringQuestionValues.length; i++) {
        questionsValue[i] = (arrStringQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('question_value', questionsValue.toString());
  }
}
